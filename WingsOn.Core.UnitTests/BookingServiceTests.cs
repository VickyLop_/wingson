﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Moq;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;
using WingsOn.Core.Services;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.Core.UnitTests {
	public class BookingServiceTests {
		readonly Mock<IBookingRepository> bookingRepo = new Mock<IBookingRepository>();
		readonly Mock<IFlightRepository> flightRepo = new Mock<IFlightRepository>();
		readonly Mock<IPersonRepository> personRepo = new Mock<IPersonRepository>();
		readonly Mock<IMapper> mapper = new Mock<IMapper>();
		readonly BookingService sut;

		public BookingServiceTests() {
			sut = new BookingService(bookingRepo.Object, mapper.Object, flightRepo.Object, personRepo.Object);
		}

		[Fact]
		public void InsertBooking_ValidRequest_InsertsBooking() {
			const int BookingId = 1;
			var request = new InsertBookingRequest {
				Customer = new Person {
					Name = "customer"
				},
				Passengers = new List<Person> {
					new Person {
						Name = "passenger1"
					},
					new Person {
						Name = "passenger2"
					}
				},
				FlightNumber = "fligtNumber",
				BookingNumber = "bookingNumber",
				DateBooking = new DateTime(2010, 10, 10)
			};
			var booking = new Booking {
				Customer = request.Customer,
				Passengers = request.Passengers
			};
			var flight = new Flight();

			mapper.Setup(m => m.Map<Booking>(request)).Returns(booking);
			flightRepo.Setup(x => x.Get(request.FlightNumber)).Returns(flight);
			bookingRepo.Setup(
				x => x.Insert(
					It.Is<Booking>(
						b => b == booking
						     && b.Flight == flight))).Returns(BookingId);
			personRepo.Setup(x => x.Insert(booking.Customer)).Returns(1);
			personRepo.Setup(x => x.Insert(booking.Passengers.First())).Returns(2);
			personRepo.Setup(x => x.Insert(booking.Passengers.Last())).Returns(3);

			var result = sut.InsertBooking(request);

			personRepo.Verify(x => x.Insert(It.IsAny<Person>()), Times.Exactly(3));
			Assert.Equal(BookingId, result);
		}

		[Fact]
		public void InsertBooking_InvalidFlight_ThrowsException() {
			var request = new InsertBookingRequest {
				Customer = new Person {
					Name = "customer"
				},
				Passengers = new List<Person> {
					new Person {
						Name = "passenger1"
					},
					new Person {
						Name = "passenger2"
					}
				}
			};

			flightRepo.Setup(x => x.Get(request.FlightNumber)).Returns((Flight)null);

			Assert.Throws<NotFoundObjectException>(() => sut.InsertBooking(request));
		}

		[Fact]
		public void InsertBooking_NullRequest_ThrowException() {
			Assert.Throws<ArgumentNullException>("request", () => sut.InsertBooking(null));
		}

		[Fact]
		public void InsertBooking_EmptyCustomer_ThrowException() {
			var request = new InsertBookingRequest();
			var booking = new Booking();
			mapper.Setup(m => m.Map<Booking>(request)).Returns(booking);
			flightRepo.Setup(x => x.Get(request.FlightNumber)).Returns(new Flight());

			Assert.Throws<BadRequestException>(() => sut.InsertBooking(new InsertBookingRequest()));
		}
	}
}