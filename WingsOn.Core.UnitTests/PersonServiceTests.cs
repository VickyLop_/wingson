using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;
using WingsOn.Core.Services;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.Core.UnitTests {
	public class PersonServiceTests {
		readonly Mock<IPersonRepository> personRepo = new Mock<IPersonRepository>();
		readonly PersonService sut;

		public PersonServiceTests() {
			sut = new PersonService(personRepo.Object);
		}

		[Fact]
		public void GetPerson_ValidId_ReturnsPerson() {
			const int PersonId = 1;
			var expected = new Person();

			personRepo.Setup(x => x.Get(PersonId)).Returns(expected);

			var result = sut.GetPerson(PersonId);

			Assert.Equal(expected, result);
		}

		[Fact]
		public void UpdateEmail_ValidId_UpdateSuccessfull() {
			var person = new Person {
				Email = "old@email.com"
			};
			var request = new UpdateEmailRequest {
				Email = "new@email.com"
			};
			personRepo.Setup(x => x.Get(request.PersonId)).Returns(person).Verifiable();
			personRepo.Setup(x => x.Save(It.Is<Person>(p => p == person && p.Email == request.Email))).Verifiable();

			sut.UpdateEmail(request);

			personRepo.VerifyAll();
		}

		[Fact]
		public void UpdateEmail_InvalidId_NotFoundException() {
			var request = new UpdateEmailRequest();
			personRepo.Setup(x => x.Get(request.PersonId)).Returns((Person)null).Verifiable();

			Assert.Throws<NotFoundObjectException>(() => sut.UpdateEmail(request));

			personRepo.Verify(x => x.Save(It.IsAny<Person>()), Times.Never);
		}

		[Fact]
		public void UpdateEmail_EmptyRequest_NotFoundException() {
			Assert.Throws<ArgumentNullException>("request", () => sut.UpdateEmail(null));

			personRepo.Verify(x => x.Save(It.IsAny<Person>()), Times.Never);
		}

		[Fact]
		public void GetPersons_EmptyRequest_ReturnsAllPersons() {
			var persons = Enumerable.Range(1, 5).Select(
				x => new Person {
					Id = x
				}).ToList();
			var request = new SearchRequest();
			personRepo.Setup(x => x.GetAll()).Returns(persons);

			var result = sut.GetPersons(request);

			Assert.Equal(persons, result);
		}

		[Fact]
		public void GetPersons_FilterByGendre_ReturnsFilteredResults() {
			var persons = new List<Person> {
				new Person {
					Id = 1,
					Gender = GenderType.Female
				},
				new Person {
					Id = 2,
					Gender = GenderType.Male
				}
			};
			var request = new SearchRequest {
				Gender = GenderType.Female
			};
			personRepo.Setup(x => x.GetAll()).Returns(persons);

			var result = sut.GetPersons(request);

			Assert.Equal(1, result.Single().Id);
		}

		[Fact]
		public void GetPersons_EmptySearchCriteria_ReturnsAllPersons() {
			var persons = new List<Person> {
				new Person {
					Id = 1,
					Gender = GenderType.Female
				},
				new Person {
					Id = 2,
					Gender = GenderType.Male
				}
			};
			personRepo.Setup(x => x.GetAll()).Returns(persons);

			var result = sut.GetPersons(null);

			Assert.Equal(persons, result);
		}
	}
}