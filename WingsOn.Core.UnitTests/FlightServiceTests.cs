﻿using System;
using System.Collections.Generic;
using Moq;
using WingsOn.Core.Services;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.Core.UnitTests {
	public class FlightServiceTests {
		readonly Mock<IBookingRepository> bookingRepo = new Mock<IBookingRepository>();
		readonly FlightService sut;

		public FlightServiceTests() {
			sut = new FlightService(bookingRepo.Object);
		}

		[Fact]
		public void GetPersons_ValidFlightNumber_ReturnsListOfPersons() {
			const string FlighNumber = "AA123";
			var expected = new List<Person> {
				new Person {
					Id = 1
				},
				new Person {
					Id = 2
				}
			};
			var bookings = new List<Booking> {
				new Booking {
					Passengers = expected
				}
			};
			bookingRepo.Setup(x => x.GetBookingsByFlightNumber(FlighNumber)).Returns(bookings);

			var result = sut.GetPersons(FlighNumber);

			Assert.Equal(expected, result);
		}

		[Fact]
		public void GetPersons_NullFlightNumber_ThrowsException() {
			Assert.Throws<ArgumentNullException>("flightNumber", () => sut.GetPersons(null));
		}
	}
}