﻿using System;
using WingsOn.Domain;

namespace WingsOn.IntegrationTests {
	public static class TestData {
		public static Person[] Persons = {
			new Person {
				Id = 1,
				Name = "Female Person",
				Address = "address",
				DateBirth = new DateTime(2000, 01, 02),
				Email = "email@email.com",
				Gender = GenderType.Female
			},
			new Person {
				Id = 2,
				Name = "Male Person",
				Email = "email@email.com",
				Address = "address",
				DateBirth = new DateTime(2000, 01, 02),
				Gender = GenderType.Male
			}
		};
		public static Booking[] Bookings = {
			new Booking {
				Id = 1,
				Number = "AA-123456",
				Flight = new Flight {
					Id = 1,
					Number = "AA123"
				},
				Passengers = new[] {
					new Person {
						Id = 2,
						Name = "Passenger_AA123"
					}
				}
			},

			new Booking {
				Id = 2,
				Number = "BB-123456",
				Flight = new Flight {
					Id = 2,
					Number = "BB123"
				},
				Passengers = new[] {
					new Person {
						Id = 3,
						Name = "Passenger_BB123"
					},
					new Person {
						Id = 4,
						Name = "Passenger2_BB123"
					}
				}
			}
		};
		public static Flight[] Flights = {
			new Flight {
				Id = 1,
				Number = "CC123"
			}
		};
	}
}