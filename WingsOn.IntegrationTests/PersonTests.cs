using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WingsOn.ApiModels;
using WingsOn.Core.Models;
using WingsOn.Domain;
using WingsOn.IntegrationTests.Infrastructure;
using Xunit;
using Person = WingsOn.Domain.Person;

namespace WingsOn.IntegrationTests {
	[Collection("WingsOnApiCollection")]
	public class PersonTests : IClassFixture<WingsOnFixture> {
		readonly IWingsOnApi wingsonApi;

		public PersonTests(WingsOnFixture fixture) {
			wingsonApi = fixture.WingsOnApi;
		}

		[Fact]
		public async Task GetPerson_ValidId_ReturnsPerson() {
			var expected = new ApiModels.Person {
				Id = 1,
				Address = "address",
				DateBirth = new DateTime(2000, 01, 02),
				Email = "email@email.com",
				Gender = Gender.Female,
				Name = "Female Person"
			};
			var response = await wingsonApi.GetPersons(1);
			var content = await response.Content.ReadAsStringAsync();

			Assert.Equal(HttpStatusCode.OK, response.StatusCode);
			Assert.Equal(
				JsonConvert.SerializeObject(
					expected,
					new JsonSerializerSettings {
						ContractResolver = new CamelCasePropertyNamesContractResolver()
					}),
				content);
		}

		[Fact]
		public async Task GetPerson_InvalidId_ReturnsNotFound() {
			var response = await wingsonApi.GetPersons(999);

			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}

		[Fact]
		public async Task GetPerson_BadRequest_ReturnsBadRequest() {
			var response = await wingsonApi.GetPersons(-1);

			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}

		[Fact]
		public async Task GetPersons_ReturnsAllPersons() {
			var response = await wingsonApi.GetPersons(null);
			var content = await response.Content.ReadAsStringAsync();

			Assert.Equal(HttpStatusCode.OK, response.StatusCode);
			Assert.Equal(TestData.Persons.Count(), JsonConvert.DeserializeObject<IEnumerable<Person>>(content).Count());
		}

		[Fact]
		public async Task UpdateEmail_InvalidIdReturnsNotFound() {
			var response = await wingsonApi.UpdateEmail(
				2,
				new UpdateEmailRequest {
					Email = "new@email.com"
				});

			Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
			Assert.Equal("new@email.com", TestData.Persons.First(x => x.Id == 2).Email);
		}

		[Fact]
		public async Task GetPersons_FilterByGendre_ReturnsExpectedPersons() {
			var response = await wingsonApi.GetPersons(
				new SearchRequest {
					Gender = GenderType.Female
				});
			var content = await response.Content.ReadAsStringAsync();

			Assert.Equal(HttpStatusCode.OK, response.StatusCode);
			var persons = JsonConvert.DeserializeObject<List<Person>>(content);
			Assert.True(persons.All(x => x.Gender == GenderType.Female));
		}

		[Fact]
		public async Task GetPersons_FilterByInvalidGendre_ReturnsBadRequest() {
			var response = await wingsonApi.GetPersons(
				new SearchRequest {
					Gender = (GenderType)99
				});

			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}
	}
}