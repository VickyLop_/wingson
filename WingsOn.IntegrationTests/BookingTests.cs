﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WingsOn.ApiModels;
using WingsOn.IntegrationTests.Infrastructure;
using Xunit;

namespace WingsOn.IntegrationTests {
	[Collection("WingsOnApiCollection")]
	public class BookingTests : IClassFixture<WingsOnFixture> {
		readonly IWingsOnApi wingsonApi;

		public BookingTests(WingsOnFixture fixture) {
			wingsonApi = fixture.WingsOnApi;
		}

		[Fact]
		public async Task CreateBooking_InvalidRequest_ReturnsBadRequestResponse() {
			var response = await wingsonApi.CreateBooking(new CreateBookingRequest());

			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task CreateBooking_ValidRequest_CreatesBooking() {
			var response = await wingsonApi.CreateBooking(
				new CreateBookingRequest {
					FlightNumber = "CC123",
					BookingNumber = "CC-123456",
					Customer = new Person {
						Name = "CC_Customer",
						Gender = Gender.Female,
						Email = "cc@domain.com"
					},
					Passengers = new List<Person> {
						new Person {
							Name = "CC_Passenger1",
							Gender = Gender.Male,
							Email = "cc1@domain.com"
						},
						new Person {
							Name = "CC_Passenger2",
							Gender = Gender.Male,
							Email = "cc2@domain.com"
						}
					}
				});

			Assert.Equal(HttpStatusCode.Created, response.StatusCode);

			var fligtPassengers = await wingsonApi.GetFlightPassengers("CC123");
			var newPassenger = JsonConvert.DeserializeObject<List<Person>>(await fligtPassengers.Content.ReadAsStringAsync());
			Assert.Equal(2, newPassenger.Count);
		}

		[Fact]
		public async Task CreateBooking_InvalidFlightNumber_ReturnsNotFoundResponse() {
			var response = await wingsonApi.CreateBooking(
				new CreateBookingRequest {
					FlightNumber = "invalid_number",
					BookingNumber = "DD-123456",
					Customer = new Person {
						Name = "DD_Customer",
						Gender = Gender.Female,
						Email = "dd@domain.com"
					},
					Passengers = new List<Person> {
						new Person {
							Name = "DD_Passenger1",
							Gender = Gender.Male,
							Email = "dd1@domain.com"
						},
						new Person {
							Name = "DD_Passenger2",
							Gender = Gender.Male,
							Email = "dd2@domain.com"
						}
					}
				});
			var content = await response.Content.ReadAsStringAsync();

			var error = JsonConvert.DeserializeObject<object>(content);
			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
			Assert.Contains("Flight with number invalid_number not found", error.ToString());
		}
	}
}