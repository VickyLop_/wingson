﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Refit;
using WingsOn.Dal;

namespace WingsOn.IntegrationTests.Infrastructure {
	public class WingsOnFixture : WebApplicationFactory<Startup> {
		public IWingsOnApi WingsOnApi { get; }

		public WingsOnFixture() {
			var httpClient = CreateClient();

			WingsOnApi = RestService.For<IWingsOnApi>(httpClient);
		}

		protected override void ConfigureWebHost(IWebHostBuilder builder) {
			builder.ConfigureTestServices(
				services => {
					services.AddSingleton<IPersonRepository>(x => new PersonRepository(TestData.Persons));
					services.AddSingleton<IBookingRepository>(x => new BookingRepository(TestData.Bookings));
					services.AddSingleton<IFlightRepository>(x => new FlightRepository(TestData.Flights));
				});
		}
	}
}