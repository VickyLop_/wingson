﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Refit;
using WingsOn.ApiModels;
using WingsOn.Core.Models;

namespace WingsOn.IntegrationTests.Infrastructure {
	public interface IWingsOnApi {
		[Get("/persons/{id}")]
		Task<HttpResponseMessage> GetPersons(int id);

		[Get("/flights/{flightNumber}/passengers")]
		Task<HttpResponseMessage> GetFlightPassengers(string flightNumber);

		[Get("/persons")]
		Task<HttpResponseMessage> GetPersons([FromQuery] SearchRequest request);

		[Put("/persons/{id}/email")]
		[Headers("Content-Type: application/json")]
		Task<HttpResponseMessage> UpdateEmail(int id, [Body] UpdateEmailRequest request);

		[Post("/bookings")]
		[Headers("Content-Type: application/json")]
		Task<HttpResponseMessage> CreateBooking([Body] CreateBookingRequest request);
	}
}