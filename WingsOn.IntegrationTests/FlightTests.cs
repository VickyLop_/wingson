﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WingsOn.ApiModels;
using WingsOn.IntegrationTests.Infrastructure;
using Xunit;

namespace WingsOn.IntegrationTests {
	[Collection("WingsOnApiCollection")]
	public class FlightTests : IClassFixture<WingsOnFixture> {
		readonly IWingsOnApi wingsonApi;

		public FlightTests(WingsOnFixture fixture) {
			wingsonApi = fixture.WingsOnApi;
		}

		[Fact]
		public async Task GetPassengers_ValidFlightNumber_ReturnsPersons() {
			var response = await wingsonApi.GetFlightPassengers("BB123");
			var content = await response.Content.ReadAsStringAsync();

			var passengers = JsonConvert.DeserializeObject<List<Person>>(content);
			Assert.Equal(HttpStatusCode.OK, response.StatusCode);
			Assert.True(passengers.All(p => p.Name.Contains("BB123")));
		}

		[Fact]
		public async Task GetPassengers_InvalidFlightNumber_ReturnsEmptyResult() {
			var response = await wingsonApi.GetFlightPassengers("invalid");
			var content = await response.Content.ReadAsStringAsync();

			var passengers = JsonConvert.DeserializeObject<List<Person>>(content);
			Assert.Equal(HttpStatusCode.OK, response.StatusCode);
			Assert.Empty(passengers);
		}
	}
}