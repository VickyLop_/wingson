﻿using System;
using System.Collections.Generic;

namespace WingsOn.ApiModels {
	public class CreateBookingRequest {
		public string FlightNumber { get; set; }
		public string BookingNumber { get; set; }
		public Person Customer { get; set; }
		public List<Person> Passengers { get; set; }
		public DateTime DateBooking { get; set; }
	}
}