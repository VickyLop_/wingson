﻿namespace WingsOn.ApiModels {
	public class GetFlightPersonsRequest {
		public string FlightNumber { get; set; }
	}
}