﻿namespace WingsOn.ApiModels {
	public class UpdateEmailRequest {
		public string Email { get; set; }
	}
}