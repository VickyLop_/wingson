﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WingsOn.ApiModels;
using WingsOn.Core.Models;
using WingsOn.Core.Services.Interfaces;

namespace WingsOn.Controllers {
	[Route("persons")]
	[ApiController]
	public class PersonController : Controller {
		readonly IPersonService personService;
		readonly IMapper mapper;

		public PersonController(IMapper mapper, IPersonService personService) {
			this.mapper = mapper;
			this.personService = personService;
		}

		/// <summary>
		/// Returns a person by Id
		/// </summary>
		/// <returns>Returns person</returns>
		/// <remarks>
		/// </remarks>
		/// <response code="200">Returns person by id</response>
		/// <response code="400">Bad request</response>
		/// <response code="404">Person not found</response>
		/// <response code="500">Failed to get person</response> 
		[HttpGet("{id}")]
		[Produces("application/json")]
		[ProducesResponseType(typeof(Person), 200)]
		[ProducesResponseType(404)]
		[ProducesResponseType(500)]
		public IActionResult GetPerson(int id) {
			var person = personService.GetPerson(id);

			var result = mapper.Map<Person>(person);

			return result == null ? (IActionResult)new NotFoundResult() : new OkObjectResult(result);
		}

		/// <summary>
		/// Returns a list of persons, filters available: gender
		/// </summary>
		/// <returns>List of persons</returns>
		/// <remarks>
		/// </remarks>
		/// <response code="200">Returns person by id</response>
		/// <response code="400">Bad request</response> 
		/// <response code="500">Failed to get persons</response>
		[HttpGet("")]
		[Produces("application/json")]
		[ProducesResponseType(typeof(Person), 200)]
		[ProducesResponseType(404)]
		[ProducesResponseType(500)]
		public IActionResult GetPersons([FromQuery] GetPersonsRequest request) {
			var searchRequest = mapper.Map<SearchRequest>(request);

			var persons = personService.GetPersons(searchRequest);

			var result = mapper.Map<List<Person>>(persons);

			return new OkObjectResult(result);
		}

		/// <summary>
		/// Updates a person’s email
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <response code="204">Email updated</response>
		/// <response code="404">Person not found</response>
		/// <response code="500">Failed to update email</response>
		[HttpPut("{id}/email")]
		[Produces("application/json")]
		[ProducesResponseType(typeof(Person), 204)]
		[ProducesResponseType(404)]
		[ProducesResponseType(500)]
		public IActionResult UpdateEmail(int id, [FromBody] UpdateEmailRequest updateDetails) {
			if (updateDetails == null) {
				return BadRequest("invalid request");
			}

			var updateRequest = mapper.Map<Core.UpdateEmailRequest>(updateDetails);
			updateRequest.PersonId = id;

			personService.UpdateEmail(updateRequest);

			return new NoContentResult();
		}
	}
}