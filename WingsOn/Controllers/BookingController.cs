﻿using System.Net;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WingsOn.ApiModels;
using WingsOn.Core.Models;
using WingsOn.Core.Services.Interfaces;

namespace WingsOn.Controllers {
	[Route("bookings")]
	[ApiController]
	public class BookingController : Controller {
		readonly IBookingService bookingService;
		readonly IMapper mapper;

		public BookingController(IBookingService bookingService, IMapper mapper) {
			this.bookingService = bookingService;
			this.mapper = mapper;
		}

		/// <summary>
		/// Creates a booking for a new passengers
		/// </summary>
		/// <returns> Returns the id of the new booking</returns>
		/// <remarks>
		/// </remarks>
		/// <response code="201">Booking created succesfully</response>
		/// <response code="400">Bad request</response>
		/// <response code="500">Failed to create booking</response>
		[HttpPost]
		[Produces("application/json")]
		[ProducesResponseType(201)]
		[ProducesResponseType(404)]
		[ProducesResponseType(500)]
		public IActionResult CreateBooking([FromBody] CreateBookingRequest request) {
			var mappedRequest = mapper.Map<InsertBookingRequest>(request);

			var booking = bookingService.InsertBooking(mappedRequest);
			if (booking == 0)
				return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

			return new CreatedResult(
				string.Empty,
				new CreateBookingResponse {
					Id = booking
				});
		}
	}
}