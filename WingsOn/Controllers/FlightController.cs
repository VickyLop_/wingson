﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WingsOn.ApiModels;
using WingsOn.Core.Services.Interfaces;

namespace WingsOn.Controllers {
	[Route("flights")]
	[ApiController]
	public class FlightController : Controller {
		readonly IFlightService flightService;
		readonly IMapper mapper;

		public FlightController(IFlightService flightService, IMapper mapper) {
			this.flightService = flightService;
			this.mapper = mapper;
		}

		/// <summary>
		/// Returns passengers from  a flight
		/// </summary>
		/// <returns>Returns a lits of persons</returns>
		/// <remarks>
		/// </remarks>
		/// <response code="200">Returns list of passengers</response>
		/// <response code="400">Bad request</response>
		/// <response code="500">Failed to get passengers</response>
		[HttpGet("{FlightNumber}/passengers")]
		[Produces("application/json")]
		[ProducesResponseType(typeof(Person), 200)]
		[ProducesResponseType(404)]
		[ProducesResponseType(500)]
		public IActionResult GetPersons([FromRoute] GetFlightPersonsRequest request) {
			var person = flightService.GetPersons(request.FlightNumber);

			var result = mapper.Map<List<Person>>(person);

			return new OkObjectResult(result);
		}
	}
}