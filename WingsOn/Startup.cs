﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using WingsOn.Core.Services;
using WingsOn.Core.Services.Interfaces;
using WingsOn.Dal;
using WingsOn.Ioc;

[assembly: ApiController]

namespace WingsOn {
	public class Startup {
		public IConfiguration Configuration;

		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services) {
			services.AddMvc()
				.AddFluentValidation(
					fvc =>
						fvc.RegisterValidatorsFromAssemblyContaining<Startup>())
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
				.ConfigureApiBehaviorOptions(options => { options.SuppressUseValidationProblemDetailsForInvalidModelStateResponses = false; })
				.AddJsonOptions(
					options =>
						options.SerializerSettings.Converters.Add(new StringEnumConverter()));

			services.AddSwaggerDocument(
				config =>
					config.PostProcess = document => {
						document.Info.Version = "v1";
						document.Info.Title = "WingsOn API";
						document.Info.Description = "WingsOn passengers information API";
						document.Info.TermsOfService = "None";
						document.Info.Contact = new NSwag.SwaggerContact {
							Name = "Victoria Lopez",
							Email = "vlopezclemente@gmail.com"
						};
					});

			services.AddSingleton<IFlightRepository, FlightRepository>();
			services.AddSingleton<IBookingRepository, BookingRepository>();
			services.AddSingleton<IPersonRepository, PersonRepository>();

			services.AddTransient<IPersonService, PersonService>();
			services.AddTransient<IFlightService, FlightService>();
			services.AddTransient<IBookingService, BookingService>();

			services.AddTransient(sp => new MapperBootstrap().CreateMapper());
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}
			else {
				app.UseHsts();
			}

			app.UseSwagger();
			app.UseSwaggerUi3();

			app.UseHttpsRedirection();
			app.UseMiddleware(typeof(ErrorHandlingMiddleware));
			app.UseMvc();
		}
	}
}