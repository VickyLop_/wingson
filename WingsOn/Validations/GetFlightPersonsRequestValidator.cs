﻿using FluentValidation;
using WingsOn.ApiModels;

namespace WingsOn.Validations {
	public class GetFlightPersonsRequestValidator : AbstractValidator<GetFlightPersonsRequest> {
		public GetFlightPersonsRequestValidator() {
			RuleFor(booking => booking.FlightNumber).NotEmpty();
		}
	}
}