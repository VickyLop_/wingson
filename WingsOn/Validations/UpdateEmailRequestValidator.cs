﻿using FluentValidation;
using WingsOn.ApiModels;

namespace WingsOn.Validations {
	public class UpdateEmailRequestValidator : AbstractValidator<UpdateEmailRequest> {
		public UpdateEmailRequestValidator() {
			RuleFor(person => person.Email).EmailAddress().NotEmpty();
		}
	}
}