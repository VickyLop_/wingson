﻿using FluentValidation;
using WingsOn.ApiModels;

namespace WingsOn.Validations {
	public class PersonValidator : AbstractValidator<Person> {
		public PersonValidator() {
			RuleFor(person => person.Name).NotEmpty();
			RuleFor(person => person.Email).EmailAddress().NotEmpty();
			RuleFor(person => person.Gender).IsInEnum();
		}
	}
}