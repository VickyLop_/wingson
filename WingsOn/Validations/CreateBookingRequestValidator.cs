﻿using System.Linq;
using FluentValidation;
using WingsOn.ApiModels;

namespace WingsOn.Validations {
	public class CreateBookingRequestValidator : AbstractValidator<CreateBookingRequest> {
		public CreateBookingRequestValidator() {
			RuleFor(booking => booking.BookingNumber).NotEmpty();
			RuleFor(booking => booking.Customer).NotEmpty().SetValidator(new PersonValidator());
			RuleFor(booking => booking.FlightNumber).NotEmpty();
			RuleFor(booking => booking.Passengers).Must(p => p != null && p.Any()).WithMessage("Booking must have at least one passenger");

			RuleForEach(booking => booking.Passengers).SetValidator(new PersonValidator());
		}
	}
}