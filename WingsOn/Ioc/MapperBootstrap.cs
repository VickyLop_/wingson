﻿using AutoMapper;
using WingsOn.ApiModels;
using WingsOn.Core.Models;
using WingsOn.Domain;
using Person = WingsOn.ApiModels.Person;

namespace WingsOn.Ioc {
	public class MapperBootstrap {
		public MapperConfiguration MapperConfiguration { get; private set; }

		public IMapper CreateMapper() {
			MapperConfiguration = new MapperConfiguration(
				cfg => {
					cfg.CreateMap<Domain.Person, Person>();
					cfg.CreateMap<GetPersonsRequest, SearchRequest>();
					cfg.CreateMap<UpdateEmailRequest, Core.UpdateEmailRequest>()
						.ForMember(dest => dest.PersonId, opt => opt.Ignore());
					cfg.CreateMap<CreateBookingRequest, InsertBookingRequest>();
					cfg.CreateMap<InsertBookingRequest, Booking>()
						.ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.BookingNumber))
						.ForMember(dest => dest.Flight, opt => opt.Ignore())
						.ForMember(dest => dest.Id, opt => opt.Ignore());
				});

			return MapperConfiguration.CreateMapper();
		}
	}
}