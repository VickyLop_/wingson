﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;

namespace WingsOn.Ioc {
	public class ErrorHandlingMiddleware {
		readonly RequestDelegate next;
		readonly ILogger<ErrorHandlingMiddleware> logger;
		static readonly Dictionary<Type, int?> StatusCodeMap = new Dictionary<Type, int?> {
			{
				typeof(NotFoundObjectException), StatusCodes.Status404NotFound
			}, {
				typeof(BadRequestException), StatusCodes.Status400BadRequest
			}
		};

		public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger) {
			this.next = next;
			this.logger = logger;
		}

		public async Task Invoke(HttpContext context) {
			try {
				await next(context);
			}
			catch (Exception ex) {
				logger.LogError($"Something went wrong: {ex}");
				await HandleExceptionAsync(context, ex);
			}
		}

		static Task HandleExceptionAsync(HttpContext context, Exception exception) {
			StatusCodeMap.TryGetValue(exception.GetType(), out var code);

			var result = new ErrorResult {
				Code = code ?? StatusCodes.Status500InternalServerError,
				Error = exception.Message
			};

			context.Response.ContentType = "application/json";
			context.Response.StatusCode = code ?? StatusCodes.Status500InternalServerError;
			return context.Response.WriteAsync(JsonConvert.SerializeObject(result));
		}
	}
}