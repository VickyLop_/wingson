﻿using WingsOn.Domain;
using Xunit;

namespace WingsOn.Dal.UnitTests {
	public class FlightRepositoryTests {
		readonly FlightRepository sut;
		readonly Flight[] flights = {
			new Flight {
				Id = 1,
				Number = "flight1"
			},
			new Flight {
				Id = 2,
				Number = "flight2"
			}
		};

		public FlightRepositoryTests() {
			sut = new FlightRepository(flights);
		}

		[Fact]
		public void Get_ValidFlightNumber_ReturnExpectedFlight() {
			var result = sut.Get("flight2");

			Assert.Equal(flights[1], result);
		}
	}
}