﻿using System.Collections.Generic;

namespace WingsOn.Dal.UnitTests.TestHelpers
{
	public class TestRepositoryBase : RepositoryBase<TestDomainObject> {
			public TestRepositoryBase() {
				Repository = new List<TestDomainObject> {
					new TestDomainObject {
						Id = 1,
						Name = "obj1"
					}
				};
			}
	}
}