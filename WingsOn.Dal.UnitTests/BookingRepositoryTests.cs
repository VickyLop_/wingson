﻿using System.Linq;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.Dal.UnitTests {
	public class BookingRepositoryTests {
		readonly BookingRepository sut;
		readonly Booking[] bookings = {
			new Booking {
				Id = 1,
				Flight = new Flight {
					Number = "flightNumber1"
				}
			},
			new Booking {
				Id = 2,
				Flight = new Flight {
					Number = "flightNumber2"
				}
			}
		};

		public BookingRepositoryTests() {
			sut = new BookingRepository(bookings);
		}

		[Fact]
		public void GetBookingsByFlightNumber_ValidFlightNumber_ReturnExpectedBookings() {
			var result = sut.GetBookingsByFlightNumber("flightNumber1");

			Assert.Equal(bookings[0], result.Single());
		}
	}
}