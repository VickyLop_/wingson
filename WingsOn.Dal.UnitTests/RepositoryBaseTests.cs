using System;
using WingsOn.Dal.UnitTests.TestHelpers;
using Xunit;

namespace WingsOn.Dal.UnitTests {
	public class RepositoryBaseUnitTests {
		readonly TestRepositoryBase sut;

		public RepositoryBaseUnitTests() {
			sut = new TestRepositoryBase();
		}

		[Fact]
		public void Insert_ValidDomainObject_InsertsSuccesfully() {
			var domainObj = new TestDomainObject {
				Name = "obj2"
			};
			sut.Insert(domainObj);

			Assert.Contains(sut.GetAll(), x => x.Id == 2);
		}

		[Fact]
		public void Insert_NullObject_ThrowsException() {
			Assert.Throws<ArgumentNullException>("element", () => sut.Insert(null));
		}
	}
}