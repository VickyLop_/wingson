﻿using WingsOn.Domain;

namespace WingsOn.Dal {
	public interface IPersonRepository : IRepository<Person> {
	}
}