﻿using WingsOn.Domain;

namespace WingsOn.Dal {
	public interface IFlightRepository : IRepository<Flight> {
		Flight Get(string flightNumber);
	}
}