﻿using System.Collections.Generic;
using WingsOn.Domain;

namespace WingsOn.Dal {
	public interface IBookingRepository : IRepository<Booking> {
		IEnumerable<Booking> GetBookingsByFlightNumber(string flightNumber);
	}
}