# WingsOn

WingsOn Rest-ful Web API to manage passengers information

## Getting Started:

WingsOn API is implemented in .Net Core 2.2. 


### Prerequisites:

.Net Core (>= 2.2) 

(https://dotnet.microsoft.com/download)

### Build the app:

From the WingsOn project: 

`dotnet build`

### Run the app:

From the WingsOn project: 

`dotnet run`
 
The app will be listening on https://localhost:5001


# WingsOn API

API documentation available with swagger : https://localhost:5001/swagger/

To improve: 
- Used PUT to update the email instead of Patch and JsonPatchDocument. 
- Pagination and caching would be needed to handle big volumes of data.
- Add tests for attributes and DAL project



## Get person by id

Gets person by id

### Request

`GET /persons/{id}`

    curl -X GET "https://localhost:5000/persons/91" -H "accept: application/json"
    
### Response
    
    HTTP/1.1 200 OK
    Content-Type: application/json
    
    {
      "name": "Kendall Velazquez",
      "dateBirth": "1980-09-24T00:00:00",
      "gender": "Male",
      "address": "805-1408 Mi Rd.",
      "email": "email@domain.com",
      "id": 91
    }

## Get persons

Get all persons. Optionally can be filtered by gender 

### Request

`GET /persons`

    curl -X GET "https://localhost:5000/persons?gender=male" -H "accept: application/json"
    
### Response
    
    HTTP/1.1 200 OK
    Content-Type: application/json
    
    [
      {
        "name": "Branden Johnston",
        "dateBirth": "1940-01-01T00:00:00",
        "gender": "Male",
        "address": "P.O. Box 795, 1956 Odio. Rd.",
        "email": "egestas.lacinia@Proinmi.com",
        "id": 77
      }
      ...
    ]

## Update person email

Updates email address for the specified person.

### Request
`PUT /persons/{id}/email`

    curl -X PUT "https://localhost:5000/persons/91/email" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"email\": \"email@domain.com\"}"
    
### Response
    
    HTTP/1.1 204 NoContent
    Content-Type: application/json


## Get flight passengers 

Get all passengers for the given flight

### Request
`GET /flights/{FlightNumber}/passengers`

    curl -X GET "https://localhost:5000/flights/BB124/passengers" -H "accept: application/json"
### Response
    
    HTTP/1.1 200 OK
    Content-Type: application/json


    [
      {
        "name": "Bonnie Rice",
        "dateBirth": "1977-11-16T00:00:00",
        "gender": "Male",
        "address": "3 Macpherson Junction",
        "email": "brice5@hostgator.com",
        "id": 40
      },
     ...
    ]

## Create booking

Create a booking for the specified customer and passengers. 
Required fields: 
- Flight number
- Booking number
- Customer
- Date of Booking

### Request
`POST /bookings`

    curl -X POST "https://localhost:5000/bookings" -H "accept: application/json" -H "Content-Type: application/json" 
    -d "{ \"flightNumber\": \"string\", \"bookingNumber\": \"string\", \"customer\": { \"name\": \"string\", \"dateBirth\": \"2019-03-19T19:14:26.401Z\", \"gender\": \"Male\", \"address\": \"string\", \"email\": \"string\" }, \"passengers\": [ { \"name\": \"string\", \"dateBirth\": \"2019-03-19T19:14:26.401Z\", \"gender\": \"Male\", \"address\": \"string\", \"email\": \"string\", \"id\": 0 } ], \"dateBooking\": \"2019-03-19T19:14:26.401Z\"}"


### Response
    
    HTTP/1.1 201 Created
    Content-Type: application/json

    {"id": 90}




