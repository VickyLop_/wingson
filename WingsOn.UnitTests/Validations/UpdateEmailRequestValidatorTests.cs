﻿using FluentValidation.TestHelper;
using WingsOn.Validations;
using Xunit;

namespace WingsOn.UnitTests.Validations {
	public class UpdateEmailRequestValidatorTests {
		readonly UpdateEmailRequestValidator sut;

		public UpdateEmailRequestValidatorTests() {
			sut = new UpdateEmailRequestValidator();
		}

		[Theory]
		[InlineData("")]
		[InlineData(null)]
		[InlineData("invalid_email_format")]
		public void UpdateEmailRequestValidator_NullEmail_ReturnsErrorr(string input) {
			sut.ShouldHaveValidationErrorFor(person => person.Email, input);
		}
	}
}