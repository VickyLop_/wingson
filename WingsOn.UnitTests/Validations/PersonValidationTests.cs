﻿using System;
using FluentValidation.TestHelper;
using WingsOn.ApiModels;
using WingsOn.Validations;
using Xunit;

namespace WingsOn.UnitTests.Validations {
	public class PersonValidationTests {
		readonly PersonValidator sut;

		public PersonValidationTests() {
			sut = new PersonValidator();
		}

		[Theory]
		[InlineData("")]
		[InlineData(null)]
		public void PersonValidator_NullRequiredFields_ReturnsError(string input) {
			sut.ShouldHaveValidationErrorFor(person => person.Name, input);
			sut.ShouldHaveValidationErrorFor(person => person.Email, input);
		}

		[Fact]
		public void PersonValidator_InvalidGenderEnum_ReturnsError() {
			sut.ShouldHaveValidationErrorFor(person => person.Gender, (Gender)99);
		}

		[Fact]
		public void PersonValidator_ValidData_DonNotReturnsError() {
			sut.ShouldNotHaveValidationErrorFor(person => person.Name, "name");
			sut.ShouldNotHaveValidationErrorFor(person => person.Email, "email@domain.com");
			sut.ShouldNotHaveValidationErrorFor(person => person.Address, "address");
			sut.ShouldNotHaveValidationErrorFor(person => person.Gender, Gender.Female);
			sut.ShouldNotHaveValidationErrorFor(person => person.DateBirth, DateTime.Now);
		}
	}
}