﻿using FluentValidation.TestHelper;
using WingsOn.Validations;
using Xunit;

namespace WingsOn.UnitTests.Validations {
	public class GetFlightPersonsRequestValidatorTests {
		readonly GetFlightPersonsRequestValidator sut;

		public GetFlightPersonsRequestValidatorTests() {
			sut = new GetFlightPersonsRequestValidator();
		}

		[Theory]
		[InlineData("")]
		[InlineData(null)]
		public void GetFlightPersonsRequestValidator_EmptyFlightNumber_ReturnsError(string flightnumber) {
			sut.ShouldHaveValidationErrorFor(flight => flight.FlightNumber, flightnumber);
		}
	}
}