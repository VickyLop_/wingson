﻿using System;
using System.Collections.Generic;
using FluentValidation.TestHelper;
using WingsOn.ApiModels;
using WingsOn.Validations;
using Xunit;

namespace WingsOn.UnitTests.Validations {
	public class CreateBookingRequestValidatorTests {
		readonly CreateBookingRequestValidator sut;

		public CreateBookingRequestValidatorTests() {
			sut = new CreateBookingRequestValidator();
		}

		[Fact]
		public void CreateBookingRequestValidator_NullRequiredFields_ReturnsError() {
			sut.ShouldHaveValidationErrorFor(booking => booking.BookingNumber, (string)null);
			sut.ShouldHaveValidationErrorFor(booking => booking.Customer, (Person)null);
			sut.ShouldHaveValidationErrorFor(booking => booking.FlightNumber, (string)null);
			sut.ShouldHaveValidationErrorFor(booking => booking.Passengers, (List<Person>)null);
		}

		[Fact]
		public void CreateBookingRequestValidator_EmptyRequiredFields_ReturnsError() {
			sut.ShouldHaveValidationErrorFor(booking => booking.BookingNumber, "");
			sut.ShouldHaveValidationErrorFor(booking => booking.FlightNumber, "");
			sut.ShouldHaveValidationErrorFor(booking => booking.Passengers, new List<Person>());
		}

		[Fact]
		public void CreateBookingRequestValidator_ValidData_DonNotReturnsError() {
			sut.ShouldNotHaveValidationErrorFor(booking => booking.BookingNumber, "bookingNumber");
			sut.ShouldNotHaveValidationErrorFor(booking => booking.FlightNumber, "flightNumber");
			sut.ShouldNotHaveValidationErrorFor(
				booking => booking.Passengers,
				new List<Person> {
					new Person()
				});
			sut.ShouldNotHaveValidationErrorFor(booking => booking.DateBooking, DateTime.Now);
		}
	}
}