﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using WingsOn.Core.Exceptions;
using WingsOn.Ioc;
using Xunit;

namespace WingsOn.UnitTests.Ioc {
	public class ErrorHandlingMiddlewareTests {
		readonly Mock<ILogger<ErrorHandlingMiddleware>> logger = new Mock<ILogger<ErrorHandlingMiddleware>>();
		readonly MemoryStream memoryStream;
		readonly HttpContext httpContext;
		ErrorHandlingMiddleware sut;
		Func<Task> nextFunction = () => throw new Exception();
		bool requestDelegateInvoked;

		public ErrorHandlingMiddlewareTests() {
			httpContext = new DefaultHttpContext();
			memoryStream = new MemoryStream();
			httpContext.Response.Body = memoryStream;
			httpContext.Response.StatusCode = 200;
			httpContext.Response.ContentType = "text/html";

			requestDelegateInvoked = false;
			sut = new ErrorHandlingMiddleware(RequestDelegateException, logger.Object);
		}

		async Task RequestDelegate(HttpContext context) {
			await Task.Run(
				() => { requestDelegateInvoked = context == httpContext; });
		}

		async Task RequestDelegateException(HttpContext context) {
			await Task.Run(nextFunction);
		}

		[Fact]
		public async Task Invoke_ThrowUndhandledException_ReturnsInternalServerError() {
			await sut.Invoke(httpContext);

			Assert.Equal(memoryStream, httpContext.Response.Body);
			Assert.Equal(500, httpContext.Response.StatusCode);
			Assert.Equal("application/json", httpContext.Response.ContentType);
		}

		[Fact]
		public async Task Invoke_ThrowNotFoundObjectException_ReturnsNotFoundResponse() {
			nextFunction = () => throw new NotFoundObjectException("");
			await sut.Invoke(httpContext);

			Assert.Equal(memoryStream, httpContext.Response.Body);
			Assert.Equal(404, httpContext.Response.StatusCode);
			Assert.Equal("application/json", httpContext.Response.ContentType);
		}

		[Fact]
		public async Task Invoke_NoUndhandledException_ContextDoesNotChange() {
			sut = new ErrorHandlingMiddleware(RequestDelegate, logger.Object);
			await sut.Invoke(httpContext);

			Assert.True(requestDelegateInvoked);
			Assert.Equal(memoryStream, httpContext.Response.Body);
			Assert.Equal(200, httpContext.Response.StatusCode);
			Assert.Equal("text/html", httpContext.Response.ContentType);
		}
	}
}