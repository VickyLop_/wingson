﻿using WingsOn.Ioc;
using Xunit;

namespace WingsOn.UnitTests.Ioc {
	public class MapperBootstrapTests {
		[Fact]
		public void MapperConfigurationIsValid() {
			var sut = new MapperBootstrap().CreateMapper();

			sut.ConfigurationProvider.AssertConfigurationIsValid();
		}
	}
}