﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using WingsOn.Core.Services;
using WingsOn.Core.Services.Interfaces;
using WingsOn.Dal;
using Xunit;

namespace WingsOn.UnitTests {
	public class StartupTests {
		readonly Startup sut;

		public StartupTests() {
			var configuration = new Mock<IConfiguration>();
			sut = new Startup(configuration.Object);
		}

		[Fact]
		public void ApiControllerAttributeAppliedToAssembly() {
			var controller = sut.GetType().Assembly.GetCustomAttribute<ApiControllerAttribute>();
			Assert.NotNull(controller);
		}

		[Fact]
		public void ConfigureServices_RequiredServicesRegisteredAndConfigured() {
			var serviceCollection = new Mock<IServiceCollection>();
			serviceCollection.Setup(m => m.GetEnumerator()).Returns(Mock.Of<IEnumerator<ServiceDescriptor>>());

			sut.ConfigureServices(serviceCollection.Object);

			VerifyServiceDescriptor<IFlightRepository>(serviceCollection, ServiceLifetime.Singleton, typeof(FlightRepository));
			VerifyServiceDescriptor<IBookingRepository>(serviceCollection, ServiceLifetime.Singleton, typeof(BookingRepository));
			VerifyServiceDescriptor<IPersonRepository>(serviceCollection, ServiceLifetime.Singleton, typeof(PersonRepository));

			VerifyServiceDescriptor<IPersonService>(serviceCollection, ServiceLifetime.Transient, typeof(PersonService));
			VerifyServiceDescriptor<IFlightService>(serviceCollection, ServiceLifetime.Transient, typeof(FlightService));
			VerifyServiceDescriptor<IBookingService>(serviceCollection, ServiceLifetime.Transient, typeof(BookingService));
		}

		static void VerifyServiceDescriptor<T>(Mock<IServiceCollection> serviceCollection, ServiceLifetime serviceLifetime, Type implementationType) {
			serviceCollection.Verify(
				m => m.Add(It.Is<ServiceDescriptor>(sd => sd.Lifetime == serviceLifetime && sd.ServiceType == typeof(T) && sd.ImplementationType == implementationType)),
				Times.Once);
		}
	}
}