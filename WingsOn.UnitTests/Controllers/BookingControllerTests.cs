﻿using System.Net;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WingsOn.ApiModels;
using WingsOn.Controllers;
using WingsOn.Core.Models;
using WingsOn.Core.Services.Interfaces;
using Xunit;

namespace WingsOn.UnitTests.Controllers {
	public class BookingControllerTests {
		readonly BookingController sut;
		readonly Mock<IMapper> mapper = new Mock<IMapper>();
		readonly Mock<IBookingService> bookingService = new Mock<IBookingService>();

		public BookingControllerTests() {
			sut = new BookingController(bookingService.Object, mapper.Object);
		}

		[Fact]
		public void CreateBooking_ValidRequest_ReturnsBooking() {
			var request = new CreateBookingRequest();
			var mapped = new InsertBookingRequest();
			const int BookingId = 1;

			mapper.Setup(m => m.Map<InsertBookingRequest>(request)).Returns(mapped);
			bookingService.Setup(bs => bs.InsertBooking(mapped)).Returns(BookingId);

			var result = sut.CreateBooking(request);

			var objectResult = Assert.IsType<CreatedResult>(result);
			var bookingId = Assert.IsType<CreateBookingResponse>(objectResult.Value);
			Assert.Equal(BookingId, bookingId.Id);
		}

		[Fact]
		public void CreateBooking_BookingNotCreated_ReturnsInternalServerError() {
			var request = new CreateBookingRequest();
			var mapped = new InsertBookingRequest();

			mapper.Setup(m => m.Map<InsertBookingRequest>(request)).Returns(mapped);
			bookingService.Setup(bs => bs.InsertBooking(mapped)).Returns(0);

			var result = sut.CreateBooking(request);

			var objectResult = Assert.IsType<StatusCodeResult>(result);
			Assert.Equal((int)HttpStatusCode.InternalServerError, objectResult.StatusCode);
		}
	}
}