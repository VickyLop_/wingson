﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WingsOn.ApiModels;
using WingsOn.Controllers;
using WingsOn.Core.Models;
using WingsOn.Core.Services.Interfaces;
using Xunit;
using Person = WingsOn.Domain.Person;
using PersonApi = WingsOn.ApiModels.Person;

namespace WingsOn.UnitTests.Controllers {
	public class PersonControllerTests {
		readonly PersonController sut;
		readonly Mock<IMapper> mapper = new Mock<IMapper>();
		readonly Mock<IPersonService> personService = new Mock<IPersonService>();

		public PersonControllerTests() {
			sut = new PersonController(mapper.Object, personService.Object);
		}

		[Fact]
		public void GetPerson_ValidId_ReturnsExpectedResult() {
			const int PersonId = 1;
			var person = new Person();
			var expected = new PersonApi();

			personService.Setup(x => x.GetPerson(PersonId)).Returns(person);
			mapper.Setup(x => x.Map<PersonApi>(person)).Returns(expected);

			var result = sut.GetPerson(PersonId);

			var objectResult = Assert.IsType<OkObjectResult>(result);
			var personResult = Assert.IsType<PersonApi>(objectResult.Value);
			Assert.Equal(expected, personResult);
		}

		[Fact]
		public void GetPerson_InvalidId_ReturnsNotFound() {
			const int PersonId = 1;
			personService.Setup(x => x.GetPerson(PersonId)).Returns((Person)null);

			var result = sut.GetPerson(PersonId);

			Assert.IsType<NotFoundResult>(result);
		}

		[Fact]
		public void UpdateEmail_ValidEmail_ReturnsNoContentResult() {
			var request = new UpdateEmailRequest {
				Email = "name@email.com"
			};
			const int PersonId = 1;
			var mappedRequest = new Core.UpdateEmailRequest();

			mapper.Setup(m => m.Map<Core.UpdateEmailRequest>(request)).Returns(mappedRequest);
			personService.Setup(x => x.UpdateEmail(It.Is<Core.UpdateEmailRequest>(r => r == mappedRequest && r.PersonId == PersonId))).Verifiable();

			var result = sut.UpdateEmail(PersonId, request);

			Assert.IsType<NoContentResult>(result);
			personService.VerifyAll();
		}

		[Fact]
		public void UpdateEmail_EmptyRequest_ReturnsBadRequest() {
			var result = sut.UpdateEmail(1, null);

			var objectResult = Assert.IsType<BadRequestObjectResult>(result);
			Assert.Equal("invalid request", objectResult.Value.ToString());
			personService.Verify(ps => ps.UpdateEmail(It.IsAny<Core.UpdateEmailRequest>()), Times.Never);
		}

		[Fact]
		public void GetPersons_ValidRequest_ReturnsPersons() {
			var request = new GetPersonsRequest {
				Gender = Gender.Female
			};
			var mapped = new SearchRequest();
			var domainModels = new List<Person>();
			var expected = new List<PersonApi> {
				new PersonApi {
					Id = 1
				}
			};
			mapper.Setup(m => m.Map<SearchRequest>(request)).Returns(mapped);
			personService.Setup(ps => ps.GetPersons(mapped)).Returns(domainModels);
			mapper.Setup(m => m.Map<List<PersonApi>>(domainModels)).Returns(expected);

			var result = sut.GetPersons(request);

			var objectResult = Assert.IsType<OkObjectResult>(result);
			var personResult = Assert.IsType<List<PersonApi>>(objectResult.Value);
			Assert.Equal(expected, personResult);
		}
	}
}