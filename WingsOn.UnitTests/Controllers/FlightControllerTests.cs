﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WingsOn.ApiModels;
using WingsOn.Controllers;
using WingsOn.Core.Services.Interfaces;
using Xunit;
using Person = WingsOn.Domain.Person;
using PersonApi = WingsOn.ApiModels.Person;

namespace WingsOn.UnitTests.Controllers {
	public class FlightControllerTests {
		readonly FlightController sut;
		readonly Mock<IMapper> mapper = new Mock<IMapper>();
		readonly Mock<IFlightService> flightService = new Mock<IFlightService>();

		public FlightControllerTests() {
			sut = new FlightController(flightService.Object, mapper.Object);
		}

		[Fact]
		public void GetPersons_ValidFlightNumber_ReturnsPersons() {
			var request = new GetFlightPersonsRequest {
				FlightNumber = "AA123"
			};
			var persons = new List<Person> {
				new Person {
					Id = 1
				}
			};
			var expected = new List<PersonApi> {
				new PersonApi {
					Id = 2
				}
			};

			flightService.Setup(x => x.GetPersons(request.FlightNumber)).Returns(persons);
			mapper.Setup(x => x.Map<List<PersonApi>>(persons)).Returns(expected);

			var result = sut.GetPersons(request);

			var objectResult = Assert.IsType<OkObjectResult>(result);
			var personResult = Assert.IsType<List<PersonApi>>(objectResult.Value);
			Assert.Equal(expected, personResult);
		}
	}
}