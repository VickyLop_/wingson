﻿using System;

namespace WingsOn.Core.Exceptions {
	public class NotFoundObjectException : Exception {
		public NotFoundObjectException(string message)
			: base(message) {
		}
	}
}