﻿using WingsOn.Domain;

namespace WingsOn.Core.Models {
	public class SearchRequest {
		public GenderType? Gender { get; set; }
	}
}