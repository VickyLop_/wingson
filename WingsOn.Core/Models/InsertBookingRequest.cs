﻿using System;
using System.Collections.Generic;
using WingsOn.Domain;

namespace WingsOn.Core.Models {
	public class InsertBookingRequest {
		public string FlightNumber { get; set; }
		public string BookingNumber { get; set; }
		public Person Customer { get; set; }
		public List<Person> Passengers { get; set; }
		public DateTime DateBooking { get; set; }
	}
}