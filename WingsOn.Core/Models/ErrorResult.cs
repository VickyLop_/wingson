﻿namespace WingsOn.Core.Models {
	public class ErrorResult {
		public int Code { get; set; }
		public string Error { get; set; }
	}
}