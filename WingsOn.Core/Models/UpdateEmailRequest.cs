﻿namespace WingsOn.Core {
	public class UpdateEmailRequest {
		public int PersonId { get; set; }
		public string Email { get; set; }
	}
}