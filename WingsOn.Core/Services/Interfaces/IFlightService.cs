﻿using System.Collections.Generic;
using WingsOn.Domain;

namespace WingsOn.Core.Services.Interfaces {
	public interface IFlightService {
		IEnumerable<Person> GetPersons(string flightNumber);
	}
}