﻿using System.Collections.Generic;
using WingsOn.Core.Models;
using WingsOn.Domain;

namespace WingsOn.Core.Services.Interfaces {
	public interface IPersonService {
		Person GetPerson(int id);

		IEnumerable<Person> GetPersons(SearchRequest request);

		void UpdateEmail(UpdateEmailRequest request);
	}
}