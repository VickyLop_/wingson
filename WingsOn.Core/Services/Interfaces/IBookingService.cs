﻿using WingsOn.Core.Models;

namespace WingsOn.Core.Services.Interfaces {
	public interface IBookingService {
		int InsertBooking(InsertBookingRequest request);
	}
}