﻿using System;
using System.Linq;
using AutoMapper;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;
using WingsOn.Core.Services.Interfaces;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Core.Services {
	public class BookingService : IBookingService {
		readonly IBookingRepository bookingRepo;
		readonly IFlightRepository flightRepo;
		readonly IPersonRepository personRepo;
		readonly IMapper mapper;

		public BookingService(IBookingRepository bookingRepo, IMapper mapper, IFlightRepository flightRepo, IPersonRepository personRepo) {
			this.bookingRepo = bookingRepo;
			this.mapper = mapper;
			this.flightRepo = flightRepo;
			this.personRepo = personRepo;
		}

		public int InsertBooking(InsertBookingRequest request) {
			if (request == null)
				throw new ArgumentNullException(nameof(request));
			if (request.Customer == null)
				throw new BadRequestException("Customer should not be empty");

			var booking = mapper.Map<Booking>(request);

			var flight = flightRepo.Get(request.FlightNumber);
			booking.Flight = flight ?? throw new NotFoundObjectException($"Flight with number {request.FlightNumber} not found");

			if (booking.Passengers.Any())
				foreach (var passenger in booking.Passengers) {
					personRepo.Insert(passenger);
				}

			personRepo.Insert(booking.Customer);
			return bookingRepo.Insert(booking);
		}
	}
}