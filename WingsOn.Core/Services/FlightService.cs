﻿using System;
using System.Collections.Generic;
using System.Linq;
using WingsOn.Core.Services.Interfaces;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Core.Services {
	public class FlightService : IFlightService {
		readonly IBookingRepository bookingRepository;

		public FlightService(IBookingRepository bookingRepository) {
			this.bookingRepository = bookingRepository;
		}

		public IEnumerable<Person> GetPersons(string flightNumber) {
			if (string.IsNullOrEmpty(flightNumber))
				throw new ArgumentNullException(nameof(flightNumber));

			var bookings = bookingRepository.GetBookingsByFlightNumber(flightNumber);

			return bookings.SelectMany(x => x.Passengers);
		}
	}
}