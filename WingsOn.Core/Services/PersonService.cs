﻿using System;
using System.Collections.Generic;
using System.Linq;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;
using WingsOn.Core.Services.Interfaces;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Core.Services {
	public class PersonService : IPersonService {
		readonly IPersonRepository personRepository;

		public PersonService(IPersonRepository personRepository) {
			this.personRepository = personRepository;
		}

		public Person GetPerson(int id) {
			return personRepository.Get(id);
		}

		public void UpdateEmail(UpdateEmailRequest request) {
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var person = personRepository.Get(request.PersonId);
			if (person == null)
				throw new NotFoundObjectException("person not found");

			person.Email = request.Email;
			personRepository.Save(person);
		}

		public IEnumerable<Person> GetPersons(SearchRequest request) {
			var persons = personRepository.GetAll();

			return request == null ? persons : persons.Where(x => request.Gender == null || x.Gender == request.Gender);
		}
	}
}